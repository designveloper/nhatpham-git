this is the first file  i added to my project  
it comes before all other files  

- view Head : git log HEAD
- view difference files : git diff 
- view only staged files: git diff --staged
- delete file : git rm `<filename>`
- rename file : git mv `<filename>` `<newfilename>`
- view diff file in a line: git diff --color-words `<filename>`
- reset head soft: git reset --soft `<commit version>` 
- reset head mixed: git reset --mixed `<commit version>` 
- reset head hard: git reset --hard `<commit version>` 
- retrieve an old version : git checkout `<commmit version>`
- remove all untracked file : git clean -f
- Fast-forwarded master to HEAD : git rebase HEAD master
- ignore tracked file: git rm --cached `<file>` , add file to `.gitignore` 
- tracking empty folders: add .gitkeep in that folder
- reference commit: 
   > parent commit: `HEAD^` , a3cb212^ , master^  
   > grandparent commit: `HEAD^^` , a3cb212^^ , master^^   
   > great-grandparent commit: `HEAD^^^` , a3cb212^^^ , master^^^  
- exploring tree list : git ls-tree HEAD,  git ls-tree master assets/, 
- create new branch: git branch `<branch name>`
- switch to other branch: git checkout `<branch name>`
- create and switch to other branch: git checkout -b `<branch name>`
- compare two branches: git diff `<branch1>`... `<branch2>`
- rename branch: git branch -m `<old-branch>`  `<-new-branch>`
- delete branch: git branch -D `<branch>`
- merge a branch : git merge `<branch>`
- merge a branch1 : git merge --ff-only `<branch>`
- merge a branch2 : git merge --no-ff `<branch>`
- add to stash : git stash save "message"
- show stash : git stash list or git stash show
- view stash by id : git stash show -p stash{`index`} 
- apply entry stash : git stash `apply` 
- clear all stash : git stash `clear` 
- add a remote repository: git remote add origin `<link :)>`
- update a remote repository: git remote ser-url origin `<new link :)>`
- view remote repository : git remote -v
- clone a repository: git clone `<link repo>`
- push and create branch on remote: git push origin `<branchName>`
- tracking remote branch: git branch --set-upstream `<branch>` `origin/<branch>`  
- view difference between local master and remote master : git diff master..origin/master
- push changes to remote repository: git push origin master
- fetch changes from remote to local : git fetch
- checkoute remote branch: git checkout -b `<branch>` origin/`<branch>`
- delete remote branch: git push origin --delete `<remote branch>`
